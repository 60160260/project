/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import static com.sun.javafx.application.PlatformImpl.exit;
import java.util.Scanner;
import jdk.nashorn.internal.ir.BreakNode;
import jdk.nashorn.internal.ir.ContinueNode;

/**
 *
 * @author informatics
 */
public class Project {

    private static String input;
    private static String username;
    private static String password;

    public static void showFirst() {
        Scanner kb = new Scanner(System.in);

        System.out.println("1.Login");
        System.out.println("2.Exit");
        System.out.println("Please input number(1,2) :");
        input = kb.next();
        if (input.equals("1")) {
            inputLogin();
        } else if (input.equals("2")) {
            exit();
        } else {
            showError();
        }

    }

    public static void inputLogin() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Username: ");
        username = kb.nextLine();
        System.out.println("Password: ");
        password = kb.nextLine();
    }

    private static void showLogin() {

    }

    private static void showError() {
        System.out.println("Error : Please input number 1 or 2");
        showFirst();

    }

    private static boolean isUsernameCorrect(String username) {
        return true;
    }

    private static boolean isPasswordCorrect(String password) {
        return false;
    }

    private static void showLoginError() {
        System.out.println("Error : Username or Password Incorrect");
        askBack();
    }

    private static void askBack() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Do you want to exit program ?");
        System.out.println("0.Yes");
        System.out.println("1.No");

        input = kb.next();
        if (input.equals("0")) {
            exit();
        } else if (input.equals("1")) {
            inputLogin();
        } else {
            showError2();
        }

    }

    public static void main(String[] args) {
        showFirst();
        checkUserandPass();

    }

    private static void showError2() {
        System.out.println("Error : Please input number 0 or 1");
        askBack();

    }

    private static void checkUserandPass() {
        if (!isUsernameCorrect(username)) {
            showLoginError();
        }
        if (!isPasswordCorrect(password)) {
            showLoginError();
        }
    }
}
